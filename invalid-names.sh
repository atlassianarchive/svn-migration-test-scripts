#!/bin/bash
repo=invalid-names
svncheckout=$repo-svn
gitcheckout=$repo-git

echo --- SVN and Git version ---
svn --version | grep -w version
git --version

rm -rf $repo $svncheckout $gitcheckout

echo --- Creating repo in `pwd`/test ---
svnadmin create $repo
url=file://`pwd`/$repo
svn co $url $svncheckout

cd $svncheckout
mkdir trunk && mkdir branches && mkdir tags

echo --- Create initial layout ---
echo on-trunk-initial > trunk/file
svn add trunk branches tags
svn ci -m"Initial commit on trunk"

echo --- Create "my branch" ---
svn copy trunk "branches/my branch"
svn ci -m"Create branchA"

echo --- Commit on branchA ---
echo on-branchA >> "branches/my branch/file"
echo on-branchA >> "branches/my branch/newfile" && svn add "branches/my branch/newfile"
svn ci -m"Commit on branchA"

echo --- Changes on trunk ---
echo on-trunk-after-branchA >> trunk/file
svn ci -m"Changes on trunk"

echo --- Create "my-branch?2" ---
svn copy trunk "branches/my-branch?2"
svn ci -m"Create my-branch?2"

echo --- Create "my-branch[3]" ---
svn copy trunk "branches/my-branch[3]"
svn ci -m"Create my-branch[3]"

echo --- Create "my-tag..." ---
svn copy trunk "tags/my-tag..."
svn ci -m"Create my-tag..."

echo --- Delete "my-branch?2" ---
svn delete "branches/my-branch?2"
svn ci -m"Create my-branch?2"

echo --- Test checkout [svn] ---
cd .. && rm -rf $svncheckout
svn co $url $svncheckout

echo --- Test checkout [git svn] ---
rm -rf $gitcheckout
git svn clone --stdlayout $url $gitcheckout

echo --- Test checkout [git svn, branch listing] ---
cd $gitcheckout
git branch -av

cd ..
echo --- SVN repository with commits accross branches ---
echo --- \* Repo: $url
echo --- \* SVN checkout: `pwd`/$svncheckout
echo --- \* GIT SVN checkout: `pwd`/$gitcheckout

