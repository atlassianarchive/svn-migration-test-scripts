#!/bin/bash
repo=private-branch
svncheckout=$repo-svn
gitcheckout=$repo-git

echo --- SVN and Git version ---
svn --version | grep -w version
git --version

rm -rf $repo $svncheckout $gitcheckout

echo --- Creating repo in `pwd`/test ---
svnadmin create $repo
url=file://`pwd`/$repo
svn co $url $svncheckout

cd $svncheckout
mkdir trunk && mkdir branches && mkdir tags

echo --- Create initial layout ---
echo on-trunk > trunk/test
svn add trunk branches tags
svn ci -m"Initial commit on trunk"

echo --- Create sample branch ---
svn copy trunk branches/branchA
svn ci -m"Create branchA"
echo on-branchA >> branches/branchA/test
svn ci -m"Commit on branchA"

echo --- Add private branch root ---
mkdir branches/private && svn add branches/private
svn ci -m"Add private branch root"

echo --- Commit on trunk ---
svn rm trunk/test
echo test > trunk/newtest && svn add trunk/newtest
svn ci -m"Commit on trunk"

echo --- Create \'private\' branch ---
echo "on-trunk after branchA creation" >> trunk/newtest
svn copy trunk branches/private/branchA
svn ci -m"Create 'private' branchA"

echo --- Test checkout [svn] ---
cd .. && rm -rf $svncheckout
svn co $url $svncheckout

echo --- Test checkout [git svn] ---
rm -rf $gitcheckout && mkdir $gitcheckout && cd $gitcheckout
git svn init --no-minimize-url --trunk=trunk --branches=branches --ignore-paths=branches/private --branches=branches/private/* --prefix=svn/ $url
git svn fetch

echo --- Test checkout [git svn, branch listing] ---
git branch -av

cd ..
echo --- SVN repository with a private branch ---
echo --- \* Repo: $url
echo --- \* SVN checkout: `pwd`/$svncheckout
echo --- \* GIT SVN checkout: `pwd`/$gitcheckout

