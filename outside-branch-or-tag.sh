#!/bin/bash
repo=outside-branches
svncheckout=$repo-svn
gitcheckout=$repo-git

echo --- SVN and Git version ---
svn --version | grep -w version
git --version

rm -rf $repo $svncheckout $gitcheckout

echo --- Creating repo in `pwd`/test ---
svnadmin create $repo
url=file://`pwd`/$repo
svn co $url $svncheckout

cd $svncheckout
mkdir trunk && mkdir branches && mkdir tags

echo --- Create initial layout ---
echo on-trunk-initial > trunk/file
svn add trunk branches tags
svn ci -m"Initial commit on trunk"

echo --- Commit on trunk ---
echo on-trunk-after-branchA >> trunk/file
svn ci -m"Changes on trunk"

echo --- Create branchA ---
svn copy trunk branches/branchA
svn ci -m"Create branchA"

echo --- More changes on trunk ---
echo on-trunk-after-branchA >> trunk/file
echo on-trunk-after-branchA > trunk/newfile-after-branchA && svn add trunk/newfile-after-branchA 
svn ci -m"Changes on trunk"

echo --- Tag trunk ---
svn copy trunk tags/tagged-trunk
svn ci -m"Create tag"

echo --- Commit outside trunk ---
echo outside-layout >> outside && svn add outside
svn ci -m"Commit outside trunk/branches/tags"

echo --- Copy commit to branchA ---
svn copy outside branches/branchA/outside
svn ci -m"Bring outside commit to branchA"

echo --- Test checkout [svn] ---
cd .. && rm -rf $svncheckout
svn co $url $svncheckout

echo --- Test checkout [git svn] ---
rm -rf $gitcheckout && mkdir $gitcheckout && cd $gitcheckout
git svn init --no-minimize-url --trunk=trunk --branches=branches --prefix=svn/ $url
git svn fetch

echo --- Test checkout [git svn, branch listing] ---
git branch -av

cd ..
echo --- SVN repository with commits accross branches ---
echo --- \* Repo: $url
echo --- \* SVN checkout: `pwd`/$svncheckout
echo --- \* GIT SVN checkout: `pwd`/$gitcheckout

