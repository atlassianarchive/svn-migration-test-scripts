#!/bin/bash
repo=recreated-tags
svncheckout=$repo-svn
gitcheckout=$repo-git
requiredsvn=1.4

echo --- SVN and Git version ---
svn --version | grep -w version
git --version

rm -rf $repo $svncheckout $gitcheckout

echo --- Creating repo in `pwd`/test ---
svnadmin create $repo
url=file://`pwd`/$repo
svn co $url $svncheckout

cd $svncheckout
mkdir trunk && mkdir branches && mkdir tags

echo --- Create initial layout ---
echo on-trunk-initial > trunk/file
svn add trunk branches tags
svn ci -m"Initial commit on trunk"

echo --- Create tag ---
svn copy trunk tags/release-1.0
svn ci -m"Create tag release-1.0"

echo --- Commit on trunk ---
echo on-trunk-modified >> trunk/file
echo on-trunk > trunk/missingfile && svn add trunk/missingfile
svn ci -m"Update release"

echo --- Recreate tag ---
svn rm tags/release-1.0
svn ci -m"Delete original tag release-1.0"
svn copy trunk tags/release-1.0
svn ci -m"Recreate tag release-1.0"

echo --- Test checkout [svn] ---
cd .. && rm -rf $svncheckout
svn co $url $svncheckout

echo --- Test conversion [git svn] ---
rm -rf $gitcheckout && mkdir $gitcheckout && cd $gitcheckout
git svn init --no-minimize-url --trunk=trunk --branches=branches --tags=tags --prefix=svn/ $url
git svn fetch
git svn --version

echo --- Test checkout [git svn, branch listing] ---
git branch -av

cd ..
echo --- SVN repository with commits accross branches ---
echo --- \* Repo: $url
echo --- \* SVN checkout: `pwd`/$svncheckout
echo --- \* GIT SVN checkout: `pwd`/$gitcheckout

