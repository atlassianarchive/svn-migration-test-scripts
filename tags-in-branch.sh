#!/bin/bash
repo=tags-within-branches
svncheckout=$repo-svn
gitcheckout=$repo-git

echo --- SVN and Git version ---
svn --version | grep -w version
git --version

rm -rf $repo $svncheckout $gitcheckout

echo --- Creating repo in `pwd`/test ---
svnadmin create $repo
url=file://`pwd`/$repo
svn co $url $svncheckout

cd $svncheckout
mkdir trunk && mkdir branches && mkdir tags

echo --- Create initial layout ---
echo on-trunk-initial > trunk/file
svn add trunk branches tags
svn ci -m"Initial commit on trunk"

echo --- Create subproject ---
mkdir -p branches/branchA/subproject
mkdir branches/branchA/subproject/trunk && mkdir branches/branchA/subproject/branches && mkdir branches/branchA/subproject/tags
echo on-subproject-trunk > branches/branchA/subproject/trunk/file
svn add branches/branchA
svn ci -m"Create subproject layout"

echo --- Changes on subproject [trunk] ---
echo on-subproject-trunk-modified >> branches/branchA/subproject/trunk/file
svn ci -m"Changes on subproject trunk"

echo --- Changes on subproject [branch] ---
svn copy branches/branchA/subproject/trunk branches/branchA/subproject/branches/branchA
echo on-subproject-branchA >> branches/branchA/subproject/branches/branchA/file
svn ci -m"Create subproject branchA"

echo --- Changes on subproject [branch] ---
svn copy branches/branchA/subproject/trunk branches/branchA/subproject/branches/branchB
echo on-subproject-branchB >> branches/branchA/subproject/branches/branchB/file
svn ci -m"Create subproject branchB"

echo --- Changes on subproject [tag] ---
svn copy branches/branchA/subproject/trunk branches/branchA/subproject/tags/tagsA
svn ci -m"Create subproject tagA"

echo --- Commit on root trunk ---
echo on-trunk-modified >> trunk/file
svn ci -m"Modification on root's trunk"

echo --- Test checkout [svn] ---
cd .. && rm -rf $svncheckout
svn co $url $svncheckout

echo --- Test conversion [git svn] ---
rm -rf $gitcheckout && mkdir $gitcheckout && cd $gitcheckout
#git svn init --no-minimize-url --trunk=trunk --branches=branches --tags=tags --prefix=svn/ $url
git svn init --no-minimize-url --trunk=trunk --branches=branches --branches=branches/branchA/subproject/branches --tags=tags --tags=branches/branchA/subproject/tags --prefix=svn/ $url
git svn fetch

echo --- Test checkout [git svn, branch listing] ---
git branch -av

cd ..
echo --- SVN repository with commits accross branches ---
echo --- \* Repo: $url
echo --- \* SVN checkout: `pwd`/$svncheckout
echo --- \* GIT SVN checkout: `pwd`/$gitcheckout

