#!/bin/bash
repo=across-branches
svncheckout=$repo-svn
gitcheckout=$repo-git

echo --- SVN and Git version ---
svn --version | grep -w version
git --version

rm -rf $repo $svncheckout $gitcheckout

echo --- Creating repo in `pwd`/test ---
svnadmin create $repo
url=file://`pwd`/$repo
svn co $url $svncheckout

cd $svncheckout
mkdir trunk && mkdir branches && mkdir tags

echo --- Create initial layout ---
echo on-trunk-initial > trunk/file
svn add trunk branches tags
svn ci -m"Initial commit on trunk"

echo --- Create branchA ---
svn copy trunk branches/branchA
svn ci -m"Create branchA"

echo --- Commit on branchA ---
echo on-branchA >> branches/branchA/file
echo on-branchA >> branches/branchA/newfile
svn ci -m"Commit on branchA"

echo --- Create on trunk ---
echo on-trunk-after-branchA >> trunk/file
svn ci -m"Changes on trunk"

echo --- Create branchB ---
svn copy trunk branches/branchB
svn ci -m"Create branchB"

echo --- Commit accross trunk, branchA and branchB ---
echo on-trunk-common >> trunk/file
echo on-branchA-common >> branches/branchA/file
echo on-branchA-common > branches/branchA/newfileA && svn add branches/branchA/newfileA
echo on-branchB-common >> branches/branchB/file
echo on-branchB-common > branches/branchB/newfileB && svn add branches/branchB/newfileB
svn ci -m"Commit across trunk/branchA/branchB"

echo --- Test checkout [svn] ---
cd .. && rm -rf $svncheckout
svn co $url $svncheckout

echo --- Test checkout [git svn] ---
rm -rf $gitcheckout && mkdir $gitcheckout && cd $gitcheckout
git svn init --no-minimize-url --trunk=trunk --branches=branches --prefix=svn/ $url
git svn fetch

echo --- Test checkout [git svn, branch listing] ---
git branch -av

cd ..
echo --- SVN repository with commits accross branches ---
echo --- \* Repo: $url
echo --- \* SVN checkout: `pwd`/$svncheckout
echo --- \* GIT SVN checkout: `pwd`/$gitcheckout

