#!/bin/bash
repo=std-layout
svncheckout=$repo-svn
gitcheckout=$repo-git

echo --- SVN and Git version ---
svn --version | grep -w version
git --version

rm -rf $repo $svncheckout $gitcheckout

echo --- Creating repo in `pwd`/test ---
svnadmin create $repo
url=file://`pwd`/$repo
svn co $url $svncheckout

cd $svncheckout
mkdir trunk && mkdir branches && mkdir tags

echo --- Create initial layout ---
echo on-trunk > trunk/test
svn add trunk branches tags
svn ci -m"Initial commit on trunk"

echo --- Create sample branch ---
svn copy trunk branches/branchA
svn ci -m"Create branchA"
echo on-branchA >> branches/branchA/test
svn ci -m"Commit on branchA"

echo --- Create sample tag ---
svn copy branches/branchA tags/branchA-1.0
svn ci -m"Tag branchA"

echo --- Additional commits ---
echo "on-trunk after branch and tag creation" >> trunk/test
svn ci -m"Commit after creating branchA and tagging it"
echo "on-branchA after tag creation" >> branches/branchA/test
svn ci -m"Commit after tagging branchA"

echo --- Test checkout [svn] ---
cd .. && rm -rf $svncheckout
svn co $url $svncheckout

echo --- Test checkout [git svn] ---
rm -rf $gitcheckout && mkdir $gitcheckout && cd $gitcheckout
git svn init --no-minimize-url --stdlayout --prefix=svn/ $url
git svn fetch

echo --- Test checkout [git svn, branch listing] ---
git branch -av

cd ..
echo --- SVN repository with standard layout ---
echo --- \* Repo: $url
echo --- \* SVN checkout: `pwd`/$svncheckout
echo --- \* GIT SVN checkout: `pwd`/$gitcheckout

