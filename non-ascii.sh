#!/bin/bash
repo=non-ascii
svncheckout=$repo-svn
gitcheckout=$repo-git
requiredsvn=1.4

echo --- SVN and Git version ---
svnversion=`svn --version | grep -w version`
if [[ $svnversion =~ " $requiredsvn" ]]
  then 
	 echo $svnversion
  else
	 echo "This script requires Subversion $requiredsvn, but was: $svnversion"
     exit 1
  fi
git --version

rm -rf $repo $svncheckout $gitcheckout

echo --- Creating repo in `pwd`/test ---
svnadmin create $repo
url=file://`pwd`/$repo
svn co $url $svncheckout

cd $svncheckout
mkdir trunk && mkdir branches && mkdir tags

echo --- Create initial layout ---
echo on-trunk-initial > trunk/file
svn add trunk branches tags
svn ci -m"Initial commit on trunk"

echo --- Create commit with files containing non-ASCII characters ---
echo on-trunk-éêïçû >> trunk/diacritics-characters
echo on-trunk-漢字測試 >> trunk/chinese-characters
echo on-trunk-اختبار مع الحروف العربية >> trunk/arabic-characters
echo on-trunk-Тест для России >> trunk/cyrillic-characters
svn add trunk/chinese-characters trunk/arabic-characters trunk/cyrillic-characters
svn ci -m"Add files containing chinese, arabic and cyrillic characters"

echo --- Create commit with files having non-ASCII names ---
mkdir trunk/non-ascii
echo on-trunk-test > "trunk/non-ascii/éêïçû"
echo on-trunk-test > "trunk/non-ascii/漢字測試"
echo on-trunk-test > "trunk/non-ascii/تبار مع الحروف العربي"
echo on-trunk-test > "trunk/non-ascii/Тест для России"
svn add trunk/non-ascii
svn ci -m"Add files with chinese, arabic and cyrillic names"

echo --- Create commit whose author has non-ASCII characters in his/her names ---
echo on-trunk-modified >> trunk/file
svn ci -m"Commit with non-ASCII characters in username" --username "éêïçû"

echo --- Create a branch with non-ASCII characters ---
svn copy trunk "branches/branch-漢字測試"
svn ci -m"Create branch-漢字測試" --username "漢字測試"

echo --- Create a tag with non-ASCII characters ---
svn copy trunk "tags/tag-ТестдляРоссии"
svn ci -m"Create tag-ТестдляРоссии" --username "ТестдляРоссии"

echo --- Test checkout [svn] ---
cd .. && rm -rf $svncheckout
svn co $url $svncheckout

echo --- Test conversion [git svn] ---
rm -rf $gitcheckout && mkdir $gitcheckout && cd $gitcheckout
git svn init --no-minimize-url --trunk=trunk --branches=branches --tags=tags --prefix=svn/ $url
git svn fetch
git svn --version

echo --- Test checkout [git svn, branch listing] ---
git branch -av

cd ..
echo --- SVN repository with commits accross branches ---
echo --- \* Repo: $url
echo --- \* SVN checkout: `pwd`/$svncheckout
echo --- \* GIT SVN checkout: `pwd`/$gitcheckout

