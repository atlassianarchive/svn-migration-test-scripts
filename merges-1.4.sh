#!/bin/bash
repo=merge-1.4
svncheckout=$repo-svn
gitcheckout=$repo-git

echo --- SVN and Git version ---
svnversion=`svn --version | grep -w version`
if [[ $svnversion =~ " 1.4." ]]
  then 
	 echo $svnversion
  else
	 echo "To test merges, this script requires Subversion 1.4, but was: $svnversion"
     exit 1
  fi
git --version

rm -rf $repo $svncheckout $gitcheckout

echo --- Creating repo in `pwd`/test ---
svnadmin create $repo
url=file://`pwd`/$repo
svn co $url $svncheckout

cd $svncheckout
mkdir trunk && mkdir branches && mkdir tags

echo --- Create initial layout ---
echo on-trunk-initial > trunk/file
svn add trunk branches tags
svn ci -m"Initial commit on trunk"

echo --- Create a feature branch  ---
svn copy trunk branches/feature
svn ci -m"Create feature branch"

echo --- Modify trunk ---
echo on-trunk-modified >> trunk/file
echo on-trunk-added > trunk/file-added-on-trunk && svn add trunk/file-added-on-trunk
svn ci -m"Change on trunk"

echo --- Modify feature ---
echo on-branch-modified >> branches/feature/file
echo on-branch-added > branches/feature/file-added-on-branch && svn add branches/feature/file-added-on-branch
svn ci -m"Change on feature branch"

echo --- Merge trunk to branch ---
svn switch $url/branches/feature
svn merge -r 2:3 $url/trunk
echo merged-trunk-to-branch > file
svn resolved file
svn ci -m"Commit merge trunk -> feature branch"

echo --- More changes on feature branch ---
echo on-branch-re-modified >> file
echo on-branch-modified >> file-added-on-branch
svn ci -m"More change on feature branch"

echo --- More changes on trunk ---
svn switch $url/trunk
echo on-trunk-re-modified >> file
echo on-trunk-modified >> file-added-on-trunk
svn ci -m"More change on trunk"

echo --- Merge back to trunk ---
svn switch $url/trunk
svn merge -r 3:7 $url/branches/feature
echo merged-back-to-trunk > file
echo merged-back-to-trunk > file-added-on-trunk
svn resolved file file-added-on-trunk
svn ci -m"Commit merge feature branch -> trunk"

echo --- Test checkout [svn] ---
cd .. && rm -rf $svncheckout
svn co $url $svncheckout

echo --- Test conversion [git svn] ---
rm -rf $gitcheckout && mkdir $gitcheckout && cd $gitcheckout
git svn init --no-minimize-url --trunk=trunk --branches=branches --tags=tags --prefix=svn/ $url
git svn fetch
git svn --version

echo --- Test checkout [git svn, branch listing] ---
git branch -av

cd ..
echo --- SVN repository with commits accross branches ---
echo --- \* Repo: $url
echo --- \* SVN checkout: `pwd`/$svncheckout
echo --- \* GIT SVN checkout: `pwd`/$gitcheckout

