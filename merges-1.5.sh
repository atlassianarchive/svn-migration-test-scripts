#!/bin/bash
repo=merge-1.5
svncheckout=$repo-svn
gitcheckout=$repo-git

echo --- SVN and Git version ---
svnversion=`svn --version | grep -w version`
if [[ $svnversion =~ " 1.5." ]] || [[ $svnversion =~ " 1.6." ]]
  then 
	 echo $svnversion
  else
	 echo "To test merges, this script requires Subversion 1.5 or 1.6, but was: $svnversion"
     exit 1
  fi
git --version

rm -rf $repo $svncheckout $gitcheckout

echo --- Creating repo in `pwd`/test ---
svnadmin create $repo
url=file://`pwd`/$repo
svn co $url $svncheckout

cd $svncheckout
mkdir trunk && mkdir branches && mkdir tags

echo --- Create initial layout ---
echo on-trunk-initial > trunk/file
svn add trunk branches tags
svn ci -m"Initial commit on trunk"

echo --- Create a feature branch  ---
svn copy trunk branches/feature
svn ci -m"Create feature branch"

echo --- Modify trunk ---
echo on-trunk-modified >> trunk/file
echo on-trunk-added > trunk/file-added-on-trunk && svn add trunk/file-added-on-trunk
svn ci -m"Change on trunk"

echo --- Create a private branch  ---
mkdir branches/private && svn add branches/private
svn copy trunk branches/private/personalbranch
svn ci -m"Create private branch"

echo --- Modify feature ---
echo on-branch-modified >> branches/feature/file
echo on-branch-added > branches/feature/file-added-on-branch && svn add branches/feature/file-added-on-branch
svn ci -m"Change on feature branch"

echo --- Merge trunk to branch ---
svn switch $url/branches/feature
svn merge --accept theirs-full $url/trunk
svn ci -m"Commit merge trunk -> feature branch"

echo --- More changes on feature branch ---
echo on-branch-re-modified >> file
echo on-branch-modified >> file-added-on-branch
svn ci -m"More change on feature branch"

echo --- More changes on private branch ---
svn switch $url/branches/private/personalbranch
echo on-private-branch-modified >> file
svn ci -m"More change on private branch"

echo --- More changes on trunk ---
svn switch $url/trunk
echo on-trunk-re-modified >> file
echo on-trunk-modified >> file-added-on-trunk
svn ci -m"More change on trunk"

echo --- Merge back to trunk ---
svn switch $url/trunk
svn merge --accept theirs-full --reintegrate $url/branches/feature
svn ci -m"Commit merge feature branch -> trunk"

echo --- Test checkout [svn] ---
cd .. && rm -rf $svncheckout
svn co $url $svncheckout

echo --- Test conversion [git svn] ---
rm -rf $gitcheckout && mkdir $gitcheckout && cd $gitcheckout
git svn init --no-minimize-url --trunk=trunk --branches=branches --branches=branches/private --tags=tags --prefix=svn/ $url
git svn fetch
git svn --version

echo --- Test checkout [git svn, branch listing] ---
git branch -av

cd ..
echo --- SVN repository with commits accross branches ---
echo --- \* Repo: $url
echo --- \* SVN checkout: `pwd`/$svncheckout
echo --- \* GIT SVN checkout: `pwd`/$gitcheckout

