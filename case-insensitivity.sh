#!/bin/bash

set -e

if ! [[ $(uname) = Darwin ]]; then
    echo 'lol no'
    exit 1
fi

dmg=case-sensitive.dmg
csroot=csf
repo=case-insensitivity
svncheckout=$repo-svn
gitcheckout=$repo-git
url=file://$PWD/$repo

echo --- SVN and Git version ---
svn --version | grep -w version
git --version

hdiutil detach $csroot 2>/dev/null || :
rm -rf $dmg $repo $svncheckout $gitcheckout $csroot

echo --- Creating case-sensitive sub-directory ---
hdiutil create -size 20m -fs HFS+ -fsargs -s $dmg
mkdir $csroot
if ! [[ -d $(tr a-z A-Z <<<$csroot) ]]; then
    echo 'This script assumes it is being run on a case-insensitive filesystem.'
    exit 1
fi
hdiutil attach -mountpoint $csroot $dmg
rm -f $dmg

cleanup() {
    echo "--- Case sensitive FS mounted at $PWD/$csroot"
    echo "--- Unmount it by running:"
    echo "      hdiutil detach $csroot"
}
trap cleanup EXIT

echo --- Creating repo in $PWD/$repo ---
svnadmin create $repo
svn co $url $csroot/$svncheckout

pushd $csroot/$svncheckout

echo --- Create initial layout ---
mkdir trunk tags branches
echo on-trunk-initial > trunk/file
echo on-trunk-initial > trunk/FILE
svn add trunk tags branches
svn ci -m "Initial commit on trunk"

echo --- Create branchA ---
svn copy trunk branches/branchA
svn ci -m "Create branchA"

echo --- Commit on branchA ---
echo on-branchA >> branches/branchA/file
echo on-branchA > branches/branchA/new-file
svn ci -m "Commit on branchA"

echo --- Create brancha ---
svn copy trunk branches/brancha
svn ci -m "Create brancha"

echo --- Commit on brancha ---
echo on-brancha >> branches/brancha/file
echo on-brancha > branches/brancha/new-file
svn ci -m "Commit on brancha"

popd

echo --- git-svn checkout on case-sensitive file system ---
rm -rf $csroot/$gitcheckout
mkdir $csroot/$gitcheckout
pushd $csroot/$gitcheckout
git svn init --no-minimize-url --trunk=trunk --branches=branches --prefix=svn/ $url
git svn fetch
popd

echo --- git-svn checkout on case-insensitive file system ---
rm -rf $gitcheckout
mkdir $gitcheckout
pushd $gitcheckout
git svn init --no-minimize-url --trunk=trunk --branches=branches --prefix=svn/ $url
git svn fetch
popd

echo
echo "--- SVN repository with file/branch names differing only by case ---"
echo "--- * Repo: $url"
echo "--- * SVN checkout: $PWD/$svncheckout"
echo "--- * GIT SVN checkout (case-sensitive): $PWD/$csroot/$gitcheckout"
echo "--- * GIT SVN checkout (case-insensitive): $PWD/$gitcheckout"
