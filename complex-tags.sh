#!/bin/bash
repo=complex-tags
svncheckout=$repo-svn
gitcheckout=$repo-git

echo --- SVN and Git version ---
svn --version | grep -w version
git --version

rm -rf $repo $svncheckout $gitcheckout

echo --- Creating repo in `pwd`/test ---
svnadmin create $repo
url=file://`pwd`/$repo
svn co $url $svncheckout

cd $svncheckout
mkdir trunk && mkdir branches && mkdir tags

echo --- Create initial layout ---
echo on-trunk-initial > trunk/file
svn add trunk branches tags
svn ci -m"Initial commit on trunk"

echo --- Create an additional commit ---
echo on-trunk-modified > trunk/file
echo on-trunk-addition > trunk/addition && svn add trunk/addition
svn ci -m"Modification and addition on trunk"

echo --- Create simple tag ---
svn copy trunk tags/simple
svn ci -m"Create a simple tag"

echo --- Create further changes ---
echo on-trunk-after-simple-tag > trunk/file
echo on-trunk-after-simple-tag > trunk/addition-after-simple-tag && svn add trunk/addition-after-simple-tag
svn ci -m"Further changes on trunk"

echo --- Create complex tag ---
svn up -r 1 trunk/file
svn copy trunk tags/complex
svn copy tags/simple/file tags/complex/newfile
svn ci -m"Create a complex tag"

echo --- Test checkout [svn] ---
cd .. && rm -rf $svncheckout
svn co $url $svncheckout

echo --- Test checkout [git svn] ---
rm -rf $gitcheckout && mkdir $gitcheckout && cd $gitcheckout
git svn init --no-minimize-url --trunk=trunk --tags=tags --prefix=svn/ $url
git svn fetch

echo --- Test checkout [git svn, branch listing] ---
git branch -av

cd ..
echo --- SVN repository with simple/complex tags ---
echo --- \* Repo: $url
echo --- \* SVN checkout: `pwd`/$svncheckout
echo --- \* GIT SVN checkout: `pwd`/$gitcheckout

