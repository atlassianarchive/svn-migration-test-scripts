#!/bin/bash
repo=private-branch
svncheckout=$repo-svn
gitcheckout=$repo-git

echo --- SVN and Git version ---
svn --version | grep -w version
git --version

rm -rf $repo $svncheckout $gitcheckout

echo --- Creating repo in `pwd`/test ---
svnadmin create $repo
url=file://`pwd`/$repo
svn co $url $svncheckout

cd $svncheckout
mkdir trunk && mkdir branches && mkdir tags

echo --- Create initial layout ---
echo on-trunk > trunk/test
svn add trunk branches tags
svn ci -m"Initial commit on trunk"

echo --- Create sample branch ---
svn copy trunk branches/branchA
svn ci -m"Create branchA"
echo on-branchA >> branches/branchA/test
svn ci -m"Commit on branchA"

echo --- Create \'private\' branch ---
echo "on-trunk after branchA creation" >> trunk/test
svn ci -m"Commit after creating branchA"
mkdir branches/private && svn add branches/private
svn copy trunk branches/private/branchB
svn ci -m"Create 'private' branchB"
echo on-branchB >> branches/private/branchB/test
svn ci -m"Commit on 'private' branchB"

echo --- Test checkout [svn] ---
cd .. && rm -rf $svncheckout
svn co $url $svncheckout

echo --- Test checkout [git svn] ---
rm -rf $gitcheckout && mkdir $gitcheckout && cd $gitcheckout
git svn init --no-minimize-url --trunk=trunk --branches=branches --ignore-paths=branches/private --branches=branches/private/* --prefix=svn/ $url
git svn fetch

echo --- Test checkout [git svn, branch listing] ---
git branch -av

cd ..
echo --- SVN repository with a private branch ---
echo --- \* Repo: $url
echo --- \* SVN checkout: `pwd`/$svncheckout
echo --- \* GIT SVN checkout: `pwd`/$gitcheckout

