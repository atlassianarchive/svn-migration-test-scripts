#!/bin/bash
repo=svngit-87
svncheckout=$repo-svn
gitcheckout=$repo-git

echo --- SVN and Git version ---
svn --version | grep -w version
git --version

rm -rf $repo $svncheckout $gitcheckout

echo --- Creating repo in `pwd`/test ---
svnadmin create $repo
url=file://`pwd`/$repo
svn co $url $svncheckout

cd $svncheckout
mkdir trunk && mkdir branches && mkdir tags

echo --- Create initial layout ---
echo on-trunk > trunk/test
svn add trunk branches tags
svn ci -m"Initial commit on trunk"

echo --- Create subdirectory ---
mkdir trunk/ActiveObjects && svn add trunk/ActiveObjects
echo test > trunk/ActiveObjects/file.txt && svn add trunk/ActiveObjects/file.txt
echo test >> trunk/test
svn ci -m"Commit on subdir"

echo --- Create tag from subdur ---
svn copy trunk/ActiveObjects tags/v0.6.2
svn ci -m"Tag subdir of trunk"

echo --- Commit on trunk ---
echo more-on-trunk > trunk/test
svn ci -m"Commit on trunk"

echo --- Create branch from tag ---
svn copy tags/v0.6.2 branches/v0.6
svn ci -m"Create branch from tag"

echo --- Test checkout [svn] ---
cd .. && rm -rf $svncheckout
svn co $url $svncheckout

echo --- Test checkout [git svn] ---
rm -rf $gitcheckout && mkdir $gitcheckout && cd $gitcheckout
git svn init --no-minimize-url --stdlayout --prefix=svn/ $url
git svn fetch

echo --- Test checkout [git svn, branch listing] ---
git branch -av

cd ..
echo --- SVN repository with standard layout ---
echo --- \* Repo: $url
echo --- \* SVN checkout: `pwd`/$svncheckout
echo --- \* GIT SVN checkout: `pwd`/$gitcheckout

